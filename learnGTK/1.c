#include <gtk/gtk.h>

void main(int argc, char *argv[])
{
    gtk_init(&argc, &argv);
    GtkWidget *window;

    window = gtk_window_new(GTK_WINDOW_TOPLEVEL);
    gtk_window_set_title(GTK_WINDOW(window), "Intro to GTK");
    gtk_container_set_border_width(GTK_CONTAINER(window), 50);

    gtk_main();
}