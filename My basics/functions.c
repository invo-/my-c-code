#include <stdio.h>

// prototype; without it this program wont working
void sayHello(char *namePointer[80]);

int main() {
    char name[80];
    char *p[80];
    *p = name;
    scanf("%s", name);
    sayHello(*p);
}

void sayHello(char *namePointer[80]) {
    printf("Hello, %s", *namePointer);
}