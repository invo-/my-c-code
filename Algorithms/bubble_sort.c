#include <stdio.h>

int sortCycle(int array[]);
void sortArray(int array[]);

void main()
{
    int array[10] = {9, 8, 7, 6, 5, 4, 3, 2, 1, 0};
    sortArray(array);
}

void sortArray(int array[])
{
    while (sortCycle(array) != 0)
        sortCycle(array);
}

int sortCycle(int array[])
{
    int counter = 0;

    for (int i = 0; i <= 9; i++)
    {
        if (array[i] > array[i+1])
        {
            int buf = array[i];
            array[i] = array[i+1];
            array[i+1] = buf;

            ++counter;
        }
        
        for (int i = 0; i < 10; i++)
            printf("%d", array[i]);
        
        printf("\n");
    }

    return counter;
}