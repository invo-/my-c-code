#include <stdio.h>

int main(void) {
    int i, j, k;      // define 3 integer variables
    i = 10;           // set i to 10
    j = i + 015;      // set j to i + 015 (octal number)
    k = j * j + 0xFF; // set k to j * j + 0xFF (hexadecimal number)

    printf("in decimal: %u\n", k);     // printf using decimal format
    printf("in hexadecimal: %x\n", k); // printf using hexadecimal format
    
    return 0;
}