int main() {
    
    /* ARRAY */
    // Starts from 0, ends at 9
    // Arrays may have 3 or more dimensions
    int a[10];


/****************************************************************************************/    
    

    /* STRUCTURE */
    // Is a set of variables, usually different types
    // define "s" as structure, which contains integer number "i" and char "c"
    struct {int i; char c;} s;
    // to assign to a member "i" of structure "s" value 6, need to write next  expression:
    s.i = 6;
    
    union {int i; char c;} u;
    // This Definition mean that you may have integer number or char, but not both


/****************************************************************************************/    


    // массив z структур struct table, каждая из которых имеет три члена, целое число i, указатель cp на символ и символ с
    struct table {
        int i;
        char *cp, c; /* pointer to a char and char */
    } z[20];         /* array of 20 structures */


/****************************************************************************************/    
    

    // имя table можно объявить как структуру struct table, 
    // которую можно использовать в последующих объявлениях:
    register struct table *p;
    // Объявляет p указателем на структуру struct table и предлагает сохранить её в register.
    // Во время выполнения программы p может указывать, 
    // например, на z[4] или на любой другой элемент в z, все 20 элементов которой
    // являются структурами типа struct table.

    // Чтобы сделать p указателем на z[4]:
    p = &z[4];
    // амерсанд (&) в качестве унарного оператора означает
    // "взять адрес того, что за ним следует". Скопировать в целочисленную переменную n 
    // значение члена i структуры, на котором указывает указатель p, можно следующим образом: 
    n = p->i;

    // Обратите внимание, что стрелка используется для доступа к члену структуры через указатель. 
    // Если мы будем использовать переменную z, то тогда мы должны использовать оператор с точкой:
    n = z[4].i;

    // Разница в том, что z [4] является структурой, и оператор точки выбирает элементы
    // из составных типов (структуры, массивы) напрямую. 
    // С помощью указателей мы не выбираем участника напрямую. 
    // Указатель предписывает сначала выбрать структуру и только потом выбрать члена этой структуры.

    typedef unsigned short int unshort;
    
    // определяет unshort как unsigned short (короткое целое число без знака). 
    // Теперь unshort может быть использован в программе как основной тип. Например,
    unshort ul, *u2, u3[5];
    // объявляет короткое целое число без знака, указатель на короткое целое число без знака и
    // массив коротких целых без знака.

/****************************************************************************************/    


    /* POINTERS */
    // are used for contain machine addresses in C.
    // They are used very and very often
    
    // Next expression defines 
    // int number i, pointer to int number pi, 
    // array "a" of 10 elements, array "b" of 10 pointers to int numbers
    // and pointer to a pointer to a int number
    int i, *pi, a[10], *b[10], **ppi;





    return 0;
}