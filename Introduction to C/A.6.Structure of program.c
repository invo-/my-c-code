
int c; // global variable. It can be used in the other file
static int g; // static global variable. It can be used only in this file.

int main() {

    extern int v; // external variable

    return 0;
}