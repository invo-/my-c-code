int main() {
   int i; /* just single integer */ 
   short int z1, z2; /* two short integer numbers */
   char c; /* one char*/
   unsigned short int k; /* single short unsigned integer */
   long flag_poll; /* 'int' may be omitted */
   register int r; /* register variable */

    /* transformation between types is allowed */
    flag_poll = i;
    /* even if i have type int and flag_poll - long*/

    /* forced transformation: 
       For example(transform integer into long): */
    // p ( (long) i);  // invalid in C99

}