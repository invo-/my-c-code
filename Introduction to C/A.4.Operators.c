#include <stdio.h>

int main(void) {
    
/****************************************/
    
    // FOR

    int i;
    int n = 10;
    int a[10];

    for (i = 0; i < n; i = i+1) a[i] = 0;
    // этот оператор устанавливает первые n элементов массива "а" равными нулю.
    // Выполнение оператора начинается с установки i в ноль (это делается вне цикла). 
    // Затем оператор повторяется до тех пор, пока i < n, выполняя при этом присваивание и
    // увеличение i.
    // Конечно, вместо оператора присвоения значения текущему элементу массива нуля может быть
    // составной оператор (блок), заключенный в фигурные скобки
    printf("%u\n", i);

/****************************************/

    // WHILE

    // the same, like "for"
    while (i < n) {
        a[i] = 0;
        i = i + 1;
    }


    // with the break (breaks also works with the for and while)
    int GGG = 0;
    int NNN = 10;
    while (GGG < NNN) {
        GGG = GGG + 1;
        while (GGG < NNN) {
            GGG = GGG + 1;
            break; // after "break" return into the previous while
        }
    }
    printf("GGG = %u\n", GGG);

/****************************************/

// DO WHILE
do {
    printf("haha");
}
while(i < 3);


/****************************************/

    // IF
    int k, y, j, l, m, x;

    // SIMPLE IF
    if (x < 0) k = 3;

    // COMPLICATED IF
    if (x > y) {
        i = 2;
        k = j + l;
    }

    // IF-ELSE
    if (x + 2 < y) {
        j = 2;
        k = j - 1;
    } else {
        m = 0;
    }

/****************************************/

    // SWITCH

    // -case- the same as else if
    // -default- the same as just else
    switch (k) {
        case 10:
            i = 10;
            break; // Stop here. do not go next.
        case 15:
            i = 8;
            break; // Stop here, do not go next.
        default:
            printf("wtf, guys");
    }


    return 0;
}