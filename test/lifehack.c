#include <stdio.h>

int func();

void main()
{
    while (func() < 10);
}

int func()
{
    int i = 0;
    i++;

    printf("%d", i);

    return i;
}