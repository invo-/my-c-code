#include <stdio.h>

void main()
{
    char c;
    int n;

    c = getchar();
    printf("%d", c); // it works fine.

    n = c;
    printf("\n%d", n);
}