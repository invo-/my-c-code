#include <stdio.h>

void main()
{
    printf("first cycle\n\n");
    for (int i = 0; i < 9; i++)
        printf("%d\n", i);

    printf("second cycle\n\n");
    for (int i = 0; i <= 9; i++)
        printf("%d\n", i);
}