#include <stdio.h>
#include <stdlib.h>

void swap(int *p_a, int *p_b, int b);

void main()
{
    int a = 1;
    int b = 2;

    int *p_a, *p_b = malloc(sizeof(int));

    p_a = &a;
    p_b = &b;

    swap(p_a, p_b, b);

    printf("%d, %d", a, b);
    //printf("%d, %d", p_a, p_b);
}

void swap(int *p_a, int *p_b, int b)
{
    p_a = &b;
}