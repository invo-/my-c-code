#include <stdio.h>

void swap(int* a, int* b);

void main()
{
    int a = 1;
    int b = 2;

    int* p_a = &a;
    int* p_b = &b;

    swap(p_a, p_b);

    printf("%d, %d", a, b);
}

void swap(int* a, int* b)
{
    a = b;
}