#include <stdio.h>

void swap(int a, int b);

void main()
{
    int a = 1;
    int b = 2;

    printf("MAIN A: %d, MAIN B: %d\n", a, b);

    swap(a, b);

    printf("MAIN A: %d, MAIN B: %d\n", a, b);
}

void swap(int a, int b)
{
    int tmp = b;

    b = a;
    a = tmp;

    printf("SWAP A: %d, SWAP B: %d\n", a, b);
}