#include <stdio.h>
#include <string.h>
#include "lore.c"

const char LAST_UPDATE[] = "03.02.2020";

char listener[6];

short int stage_of_the_game;

void handle_listener(char location[]);
void inventory();
void introduction();
void start();
void about();
void save();
void load();
int end();

void main() 
{
    printf("Hello, player!\n");
    introduction();
}

void handle_listener(char location[]) 
{
    printf("\n\nYou/%s> ", location);
    scanf("%s", listener);

    if (strcmp(listener, "/start") == 0) 
        start();
    else if(strcmp(listener, "/about") == 0)
        about();
    else if(strcmp(listener, "/load") == 0)
        load();
    else if(strcmp(listener, "/end") == 0)
        end();
    else
    {
        printf("Try to print again\n");
        handle_listener(menu);
    }
}

void inventory() 
{
    // here will be used an array
}

void introduction() 
{
    printf("\nCommand list:\n");
    printf("/about - to find out about this game\n");
    printf("/start - to start this game\n");
    printf("/load  - to load and start playing\n");
    printf("/end   - to end this game\n");

    handle_listener(menu);
}

void start() 
{
    class_selection();
}

void about() 
{
    printf("\nDeveloped by @Desasth\n");
    printf("02.02.2020\n");
    printf("Last update: %s", LAST_UPDATE);
    handle_listener("menu");
}

void save()
{
    FILE *cfg;
    remove(cfg);
    cfg = fopen("config.cfg", "wt");
    fclose(cfg);        
    
    handle_listener("menu");
}

void load()
{    
        // take the data from file 
        // load
}

int end()
{
        printf("\nCome back!");
        return 0;
}