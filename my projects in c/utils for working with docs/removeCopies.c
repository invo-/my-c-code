#include <stdio.h>
#include <stdbool.h>

char removeCopies(char file_name[]);

char main()
{
    char c;
    char file_name[30];
    int counter = 0;

    printf("Программа рассчитана на количество символов в строке не более 1024");

    printf("\nВведите название файла, который нужно очистить от повторяющихся строк. Название не должно быть более 30 символов.");
    
    while ((c = getchar()) != '\n')
    {
        if (counter > 30)
        {
            return "\nНазвание файла больше 30 символов. Укоротите, пожалуйста.";
        }

        file_name[counter] = c;

        ++counter;
    }
}

char removeCopies(char file_name[])
{
    char current_string[1024], other_string[1024], c;
    int current_string_number, total_lines, counter = 0;
    bool key = false;
    FILE *fp;
    
    if ((fp = fopen(file_name, "rw")) == NULL)
    {
        return "Открыть файл не удалось. Завершаю работу.";
    }

    // подсчёт количества строк в файле
    while (!feof(fp))
    {
        if (fgetc(fp) == '\n')
        {
            ++total_lines;            
        }

    }

    printf("Всего строк: %d", total_lines);

    total_lines -= 1;

    while (current_string_number < total_lines)
    {   
        if ((c = fgetc(fp)) != '\n')
        {
            current_string[counter] = c;
            ++counter;
        }
        else
        {
            key = true;
        }
        
        if (key == true)
        {
            while (!feof(fp))
            {}
        }

        key = false;
        
        ++current_string_number;
    }
}