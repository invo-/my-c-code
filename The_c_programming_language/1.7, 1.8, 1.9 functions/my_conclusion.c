#include <stdio.h>

void calculation(int n);

void main()
{
    int n = 3;

    calculation(n);

    printf("Local in main func: %d", n);
}

void calculation(int n)
{
    // в этой функции переменная n является копией, а не оригиналом. 
    // Она не способна изменить оригинальную n, которая находится в Main
    n++;

    printf("Calculation (local in func n) = %d\n", n);
}