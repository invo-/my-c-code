#include <stdio.h>

void main()
{
    float fahr, cels;
    int step, lower, upper;

    step = 20.0;
    lower = 0;
    upper = 300.0;

    fahr = lower;

    while (fahr <= upper)
    {
        cels = (5.0 / 9.0) * (fahr - 32.0); /* вещественные числа для большей точности*/
        printf("%3.0f %6.1f\n", fahr, cels);
        fahr += step;
    }
}