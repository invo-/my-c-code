#include <stdio.h>

void main() 
{
    int fahr, celsius;
    int lower, upper, step;

    lower = 0;   // минимальная величина
    upper = 300; // максимальная величина
    step = 20;   // величина шага

    fahr = lower;

    printf("Celsius | fahr\n");

    while (fahr <= upper)
    {
        celsius = 5 * (fahr - 32) / 9;
        printf("%d\t| %d\n", celsius, fahr);
        fahr += step;
    }

}