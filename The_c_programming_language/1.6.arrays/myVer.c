#include <stdio.h>

// программа подсчитывает количество цифр в массиве, 
// а также количество пустых мест.

void main()
{
    int arr[14] = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10};
    int empty_space, numbers, question;

    for (int i = 0; i <= 14; i++)
    {
        if (arr[i] != '\t' || arr[i] != '\n' || arr[i] != '\0')
            numbers++;
        else if (arr[i] == '\t' || arr[i] == '\n' || arr[i] == '\0')
            empty_space++;
        else
            question++;
    }

    printf("quantity of numbers: %d\n, empty_space: %d\n, question: %d\n", numbers, empty_space, question);

}