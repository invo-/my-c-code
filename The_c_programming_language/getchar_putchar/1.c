#include <stdio.h>

void main()
{
    int c;

    c = getchar();
    // Для консольного текста. Точка будет считаться концом строки
    while (c != '.')
    {
        putchar(c);
        c = getchar();  
    }
}