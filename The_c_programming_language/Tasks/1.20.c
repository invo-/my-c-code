/*
    * Exercise 1-20. 
    * Write a program detab that replaces tabs in the input
    * with the proper number of blanks to space to the next tab stop. 
    * Assume a fixed set of tab stops, 
    * say every n columns. 
    * Should n be a variable or a symbolic parameter?  
*/

#include <stdio.h>

#define TAB_SIZE 3

void main()
{
    char c, input[1024];
    int n, k = 0;

    while ((c = getchar()) != '.')
    {
        
        if (c != '\t') 
        {
            input[n] = c;
            ++n;
        }
        else 
        {
            k = n;

            while (n < k + TAB_SIZE)
            {
                input[n] = ' ';
                n++;
            }
        }
    }

    printf("%s", input);
}