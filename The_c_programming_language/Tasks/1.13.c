#include <stdio.h>

/*  Write a program to print a histogram of the lengths 
    of words in its input. It is easy to draw the histogram 
    with the bars horizontal; a vertical orientation 
    is more challenging. */

void main()
{
    char c;
    int wordLength;
    int counterLength[11] = {0, 0, 0, 0, 0, 0, 0, 0, 0, 0};


    while ((c = getchar()) != '.')
    {
        // it is stupid to do like this: if (c != '\n' || c != '\t' || c != ' ')
        if (c == '\n' || c == '\t' || c == ' ')
        {
            if (wordLength <= 10)
            {
                counterLength[wordLength - 1] += 1;
                wordLength = 0;
            }
            else 
            {
                counterLength[10] += 1;
                wordLength = 0;
            }
        }
        else 
        {
            ++wordLength;
        }
    }

    printf("| 1 | 2 | 3 | 4 | 5 | 6 | 7 | 8 | 9 | 10 | 11+ |\n"
           "|   |   |   |   |   |   |   |   |   |    |     |\n|");
    for (int i = 0; i < 11; i++)
    {
        if (i == 9)
            printf(" %d  |", counterLength[i]);    
        else if(i == 10)
            printf("  %d  |", counterLength[i]);    
        else
            printf(" %d |", counterLength[i]);
    }
}