/* 
    Write  a  program  to  remove  trailing  blanks  
    and  tabs  from  each  line  of  input,  
    and to delete entirely blank lines.  
*/

/* 
    23/2/2022 | 00:07

    By me:
    In this program I decided to use a stack (type of the data)
*/

#include <stdio.h>

void removeLastElementFromStack(char stack[]);
int addElementToStack(char stack[], char c);
void clearStack(char stack[]);

int stack_counter = 0;
char stack[1024];

void main()
{
    char c;
    
    while ((c = getchar()) != '.')
    {
        if (c == '\n')
        {
            if(stack[stack_counter] == ' ' || stack[stack_counter] == '\t')
                removeLastElementFromStack(stack);
        }
        else 
        {
            if (addElementToStack(stack, c) == 0)
                clearStack(stack); // ну а что ещё делать? чистим стак, если переполнился.
        }
    }

    printf("%s", stack);
}

int addElementToStack(char stack[], char c)
{
    stack[stack_counter] = c;
    ++stack_counter;
    
    if (stack_counter >= 1024)
        return 0;
    else
        return 1;
}

void removeLastElementFromStack(char stack[])
{
    stack[stack_counter] = 0;
    --stack_counter;
}

void clearStack(char stack[])
{
    for (int i = 0; i <= 1024; i++)
        stack[i] = 0;
    
    stack_counter = 0;
}