/* 
 * Write  a  program  to  determine  the  ranges  of  char, short, int,  
 * and  longvariables,  both  signed  and  unsigned,
 * by  printing  appropriate  values  from  standard  headers  and by direct
 * computation. 
 * Harder if you compute them: determine the ranges of the various floating-point types 
 */

#include <stdio.h>
#include <limits.h>
#include <float.h>

void main()
{
        printf("unsigned char = %ld\n", sizeof(unsigned char));
        printf("signed char =  %ld\n", sizeof(signed char));
        printf("char = %ld\n", sizeof(char));
        printf("unsigned int = %ld\n", sizeof(unsigned int));
        printf("short int = %ld\n", sizeof(short int));
        printf("int = %ld\n", sizeof(int));
        printf("long int = %ld\n", sizeof(long int));
        printf("float = %ld\n", sizeof(float));
        printf("double = %ld\n", sizeof(double));
        printf("long double = %ld\n", sizeof(long double));

}