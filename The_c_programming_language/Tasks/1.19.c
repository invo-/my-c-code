/* 
 * Write  a  function  reverse(s)  that  reverses  
 * the  character  string  s.  
 * Use  it  to  write a program 
 * that reverses its input a line at a time. 
*/ 

#include <stdio.h>

#define BUF_SIZE 100

void reverse(char string[], char resultString[], int n);

void main()
{
    char c;
    char string[BUF_SIZE], resultString[BUF_SIZE];
    int counter = 1;

    while ((c = getchar()) != '.')
    {
        string[counter] = c;
        counter++;
    }

    reverse(string, resultString, counter);

    for (int i = 0; i < counter; i++)
        printf("%c", resultString[i]);
}

void reverse(char string[], char resultString[], int counter)
{
    int i = 0;

    while (i < counter)
    {
        resultString[i] = string[counter - i];
        i++;
    }
}