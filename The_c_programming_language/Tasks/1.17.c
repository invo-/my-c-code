/* Write a program to print all input lines 
   that are longer than 80 characters */

#include <stdio.h>

#define MAXLINE 1000

void main()
{
    char buf[MAXLINE], current_line[MAXLINE];
    int longest_counter, current_counter, index_counter = 0;
    char c;

    while ((c = getchar()) != '.')
    {
        if (c != '\n')
        {
            current_line[current_counter] = c;
            current_counter++;
        }
        else 
        {
            if (current_counter > 20)
            {
                int i = 0;
                while (i <= current_counter)
                {
                    buf[index_counter + i] = current_line[i];
                    index_counter += i;
                    i++;
                }
            }
            current_counter = 0;
        }
    }

    printf("%s", buf);
}