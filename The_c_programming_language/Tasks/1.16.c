#include <stdio.h>

#define MAXLINE 1000

void copy(char from[], char to[], int n);

void main()
{
    int longest_counter, current_counter = 0;
    char c, current_line[MAXLINE], longest_line[MAXLINE];

    while ((c = getchar()) != '.')
    {
        if (c != '\n')
        {
            current_line[current_counter] = c;
            ++current_counter;
        }
        else 
        {
            if (current_counter >= longest_counter)
            {
                copy(current_line, longest_line, current_counter);
                longest_counter = current_counter;
                current_counter = 0;
            }
        }
    }

    printf("longest string: %s\n", longest_line);
    printf("length: %d", longest_counter);
}

void copy(char from[], char to[], int n) // как я понял, тут автоматически преобразуется к типу pointer?
{
    while (n >= 0)
    {
        to[n] = from[n];
        n--;
    }
}