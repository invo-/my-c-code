#include <stdio.h>

/*  Write a program to print a histogram of the frequencies of different characters in its input. */

void main()
{
    int counter[25];
    char c;

    for (int i = 0; i <= 25; i++)
        counter[i] = 0;

    while ((c = getchar()) != '.')
        counter[122 - c] += 1;

    printf("| a | b | c | d | e | f | g | h | i | k | l | m |"
           " n | o | p | q | r | s | t | u | v | w | x | y | z |\n|");

    for (int i = 0; i < 25; i++)
        printf(" %d |", counter[i]);
}