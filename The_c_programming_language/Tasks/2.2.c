/*
 * for (i=0; i < lim-1 && (c=getchar()) != '\n' && c != EOF; ++i)        
 *       s[i] = c; 
 * 
 * Exercise 2-2. Write a loop equivalent to the for loop above without using && or ||
 */

#include <stdio.h>

void main(int lim)
{
    int i = 0;
    char c;
    char s[100];

    while (i < lim-1)
    {
        if ((c = getchar()) != '\n')
        {
            if (c != EOF)
            {
                s[i] = c;
            }
        }
        ++i;
    }

}