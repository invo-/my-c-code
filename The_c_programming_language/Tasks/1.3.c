#include <stdio.h>

void main()
{
    int fahr, celsius;
    int step, lower, upper;

    step  = 20;  // длина шага
    upper = 300; // макс-значение
    lower = 0;   // мин-значение

    fahr = lower;

    printf("Celsius | Fahr\n");

    while(fahr <= upper) 
    {
        celsius = 5 * (fahr - 32) / 9;
        printf("%d\t| %d\n", celsius, fahr);
        fahr += step;
    }

    printf("%c", c);
}