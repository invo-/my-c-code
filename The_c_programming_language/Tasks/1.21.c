/* Упражнение 1.21
 * Напишите программу entab, которая бы заменяла
 * пустые строки, состоящие из одних пробелов, строками, содержащими
 * минимальное колиество табуляций и дополнительных пробелов,
 * так, чтобы заполнять то же пространство. 
 * Используйте те же параметры табуляции, что и в программе detab.
 * Если для заполнения места до следующей границы табуляции требуется 
 * один пробел или один символ табуляции, то что следует предпочесть?
 */

#include <stdio.h>
#include <stdlib.h>

#define MAX_IN_BUF 1024

void getString(char *p_buffer);
void stringHandler(char *p_buffer, int total);
void stringMover(char *p_buffer, int total, int from, int to);

void main()
{
    char buffer[MAX_IN_BUF];

    getString(buffer);
    printf("\n\nOUTPUT: \n\n%s", buffer);
}

// ready
void getString(char *p_buffer)
{
    char c;
    int total;

    while ((c = getchar()) != '.')
    {
        p_buffer[total] = c;
        total++;
    }

    stringHandler(p_buffer, total);
}

// ready
void stringHandler(char *p_buffer, int total)
{
    int i, this_line, white_spaces = 0;
    
    while (i < total)
    {
        if (p_buffer[i] == ' ' || p_buffer[i] == '\t')
            white_spaces++;

        if (p_buffer[i] == '\n')
        {
            if (white_spaces == this_line)
                stringMover(p_buffer, total, total - this_line, this_line + 1);
            
            this_line = 0;
        }

        i++;
    }
}

void stringMover(char *p_buffer, int total, int from, int to)
{
    int line_end = to;
    while (p_buffer[line_end] != '\n') line_end++;

    while (to < line_end)
    {
        p_buffer[from] = p_buffer[to];

        from++;
        to++;
    } 
}   