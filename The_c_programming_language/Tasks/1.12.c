#include <stdio.h>

// напишите программу, которая выводит по одному слову в строке

void main()
{
    char c;

    while ((c = getchar()) != '_')
    {
        if (c == '\n')
        {
            putchar('\n');
        }
        putchar(c);
    }
}