#include <stdio.h>

/*
    Действительно, объявлять переменную внутри функции, 
    как extern не обязательно (если сама глобальная находится в этом же файле)

    Если компилятор Си встречает переменную, 
    которая не была объявлена, то он предполагает, 
    что эта переменная ссылается на глобальную
*/

#define MAXLINE 1000 /* Maximum input line size */

int max;               /* maximum length seen so far */
char line[MAXLINE];    /* current input line */
char longest[MAXLINE]; /* longest line saved here*/

int getline(void);
void copy(void);

/* print longest input line; Specialized version */
void main()
{
    int len;
    extern int max; // будем юзать глобальную переменную max внутри этой функции
    extern char longest[]; // будем юзать глобальную переменную longest в этой функции

    max = 0;
    
    while ((len = getline()) > 0)
    {
        if (len > max)
        {
            max = len;
            copy();
        }
    }

    if (max > 0) /* there was a line */
        printf("%s", longest);
}

int getline(void)
{
    int c, i;
    extern char line[]; // будем юзать глобальную переменную line внутри этой функции

    for (i = 0; i < MAXLINE - 1
        && (c = getchar) != EOF && c != '\n'; ++i);
    {
        line[i] = c;
        ++i;
    }
    line[i] = '\0';
    return i;   
}

void copy(void)
{
    int i;
    extern char line[], longest[]; // будем юзать глобальные переменную line, longest внутри этой функции

    i = 0;
    while ((longest[i] = line[i]) != '\0')
        ++i;
}