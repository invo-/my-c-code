#include <stdio.h>

int main(void) {
    int a[20];
    double d[5] = {1000.0, 2.0, 3.4, 7.0, 50.0};

    // Omit the size of the array; an array just big enough to hold the initialization is created
    int c[] = {1, 3, 7};

    // to assign a single element of the array
    c[8] = 9;

    int just_int = c[2]; // = 7
    printf("just_int = %u\n", just_int);
    
    // filling out the array with the help of "for" cycle
    for (int i = 0; i < 10; i++) {
        c[i] = i;
        printf("c[%u] = %u\n", i, i);
    }
    

    // TWO DIMENSIONAL ARRAYS
    int twoDimensional[3][4] = {
        {0, 1, 2, 3}, /*  initializers for row indexed by 0 */
        {4, 5, 6, 7}, /*  initializers for row indexed by 1 */
        {8, 9, 10, 11} /*  initializers for row indexed by 2 */
    };
    // IS EQUIVALENT TO THE PREVIOUS EXAMPLE
    int twoDimensionalSecond[3][4] = {0,1,2,3,4,5,6,7,8,9,10,11};

    // An element in a two-dimensional array is accessed by using the subscripts,
    // i.e., row index and column index of the array
    int val = twoDimensional[2][3];


}